import { Component, ViewChild } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { DarkmodeService } from './shared/service/darkmode.service';
import { MotifsService } from './shared/service/motifs.service';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import { APPDL } from '../environments/environment';

import { faDownload } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['initialisation/app.component.scss']
})
export class AppComponent {

  modalRef: BsModalRef;
  @ViewChild('updtModal')updtModal;
  APPDL = APPDL;
  faDownload = faDownload;

      constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private darkmode: DarkmodeService,
    private motifsService: MotifsService,
    private modalService: BsModalService,
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.motifsService.init()
          .then(update => {
            if (!update){
              console.log('yousk2');
              this.modalRef = this.modalService.show(this.updtModal);
            }
          })
    });
  }
}

// affiche si un champ de formulaire est valide ou non
export function setclassinput(userform, input) {
  let css = '';
  if (userform.get(input).touched && userform.get(input).errors) {
    css = 'is-invalid';
  } else if (!userform.get(input).errors) {
    css = 'is-valid';
  }
  return css;
};
