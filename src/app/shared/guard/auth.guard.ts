import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import { Observable } from 'rxjs';

import { AuthService } from '../service/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router,
              private auth: AuthService) {}

  canActivate(
      next: ActivatedRouteSnapshot,
      state: RouterStateSnapshot
  ): boolean {
    // L'utilisateur est-il identifié ?
    if (this.auth.isAuthenticated()) {
      return true;
    } else {
      // Non, on le redirige sur la page par défaut
      this.router.navigate(['/initialisation']);
      return false;
    }
  }
}
