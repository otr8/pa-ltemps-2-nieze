import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DarkmodeService {

  public darkmode = false;

  constructor() {
    this.darkmode = JSON.parse(localStorage.getItem('darkMode'));
    if (this.darkmode){
      document.body.classList.add('dark');
    }
  }

  public toggle(){
    if (this.darkmode){
      this.darkmode = false;
      document.body.classList.remove('dark');
    }else{
      this.darkmode = true;
      document.body.classList.add('dark');
    }
    localStorage.setItem('darkMode', JSON.stringify(this.darkmode));
  }
}
