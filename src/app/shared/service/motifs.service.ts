import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { API } from '../../../environments/environment';
import { faSuitcase, faShoppingBasket, faNotesMedical, faUsers, faWheelchair, faPaw, faFileSignature, faHandHoldingHeart, faSchool, faCar } from '@fortawesome/free-solid-svg-icons';
import { version } from '../../../../package.json';

@Injectable({
  providedIn: 'root'
})
export class MotifsService {

  faFont = {
    faSuitcase,
    faShoppingBasket,
    faNotesMedical,
    faUsers,
    faWheelchair,
    faPaw,
    faFileSignature,
    faHandHoldingHeart,
    faSchool,
    faCar
  };
  private _motifsdefauts = [
    {
      titre: 'Travail',
      desc: 'Déplacements entre le domicile et le lieu d’exercice de l’activité professionnelle ou un établissement d’enseignement ou de formation, déplacements professionnels ne pouvant être différés, déplacements pour un concours ou un examen. A utiliser par les travailleurs non salariés, lorsqu\'ils ne peuvent disposer d\'un justificatif de déplacement établi par leur employeur.',
      icon: 'faSuitcase',
      color: '#673ab7',
      value: 'travail',
      active: true
    },
    {
      titre: 'Achats',
      desc: 'Déplacements pour se rendre dans un établissement culturel autorisé ou un lieu de culte ; déplacements pour effectuer des achats de biens, pour des services dont la fourniture est autorisée, pour les retraits de commandes et les livraisons à domicile.',
      icon: 'faShoppingBasket',
      color: '#795548',
      value: 'achats',
      active: true
    },
    {
      titre: 'Santé',
      desc: 'Consultations, examens et soins ne pouvant être assurés à distance et l’achat de médicaments.',
      icon: 'faNotesMedical',
      color: '#4caf50',
      value: 'sante',
      active: true
    },
    {
      titre: 'Famille',
      desc: 'Déplacements pour motif familial impérieux, pour l\'assistance aux personnes vulnérables et précaires ou la garde d\'enfants.',
      icon: 'faUsers',
      color: '#f44336',
      value: 'famille',
      active: true
    },
    {
      titre: 'Handicap',
      desc: 'Déplacement des personnes en situation de handicap et leur accompagnant.',
      icon: 'faWheelchair',
      color: '#2196f3',
      value: 'handicap',
      active: true
    },
    {
      titre: 'Sport / animaux',
      desc: 'Déplacements en plein air ou vers un lieu de plein air, sans changement du lieu de résidence, dans la limite de trois heures quotidiennes et dans un rayon maximal de vingt kilomètres autour du domicile, liés soit à l’activité physique ou aux loisirs individuels, à l’exclusion de toute pratique sportive collective et de toute proximité avec d’autres personnes, soit à la promenade avec les seules personnes regroupées dans un même domicile, soit aux besoins des animaux de compagnie.',
      icon: 'faPaw',
      color: '#ffc107',
      value: 'sport_animaux',
      active: true
    },
    {
      titre: 'Convocation',
      desc: 'Convocation judiciaire ou administrative et pour se rendre dans un service public.',
      icon: 'faFileSignature',
      color: '#3f51b5',
      value: 'convocation',
      active: true
    },
    {
      titre: 'Missions',
      desc: 'Participation à des missions d\'intérêt général sur demande de l\'autorité administrative',
      icon: 'faHandHoldingHeart',
      color: '#cddc39',
      value: 'missions',
      active: true
    },
    {
      titre: 'Enfants',
      desc: 'Déplacement pour chercher les enfants à l’école et à l’occasion de leurs activités périscolaires.',
      icon: 'faSchool',
      color: '#e91e63',
      value: 'enfants',
      active: true
    },
  ];

  private motifs: any;
  private update = true;

  constructor(
      private http: HttpClient,
  ) {}

  public async init(): Promise<boolean>{
    const localMotifs = JSON.parse(localStorage.getItem('motifs'));
    if (localMotifs === null){
      this.motifs = this._motifsdefauts;
    }else{
      this.motifs = localMotifs;
    }

    await this.http.get(API).toPromise()
        .then((data: any) => {
          this.motifs = data.motifs;
          localStorage.setItem('motifs', JSON.stringify(data.motifs));
          this.verifUpdate(data.version);
        }, error => {
          console.log(error);
        });
    return this.update;
  }

  public getMotifs(): any {
    return this.motifs;
  }

  public getIcon(icon: string): any{
    return this.faFont[icon];
  }

  public verifUpdate(serv: string): void{
      if (version !== serv){
        this.update = false;
      }
  }
}
