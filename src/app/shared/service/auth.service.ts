import { Injectable } from '@angular/core';
import {AlertController} from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
      public alertCtrl: AlertController,
  ) {
    this.users = JSON.parse(localStorage.getItem('users'));
    if (this.users === null){
    }
  }

  private users: [User];
  private tempUser = new User();

  private savUsers(){
    localStorage.setItem('users', JSON.stringify(this.users));
  }

  public isAuthenticated(): boolean {
    return this.users !== null;
  }

  public registrer(prenom: string, nom: string, dateNaissance: string, lieuNaissance: string, adresse: string, ville: string, codePostal: string, label: string){
    const id = prenom[0] + nom[0] + Math.floor(Math.random() * 999999);
    this.tempUser = {
      id,
      prenom,
      nom,
      dateNaissance,
      lieuNaissance,
      adresse,
      ville,
      codePostal,
      label,
    };

    if (this.users === null){
      this.users = [this.tempUser];
    }else{
      this.users.push(this.tempUser);
    }
    this.savUsers();
    return id;
  }

  public getUsers(): [User]{
    return this.users;
  }

  public getUser(id): any{
    for (let i = 0; i < this.users.length; i++){
      if (this.users[i].id === id){
        return this.users[i];
      }
    }
    return false;
  }

  public removeUser(id){
    if(this.users.length <= 1){
      this.error('Vous ne pouvez pas supprimer votre dernier utilisateur');
      return;
    }

    const unique = JSON.parse(localStorage.getItem('attest_unique'));
    if(unique !== null){
      let newUnique = [];
      for (var i = 0; i < unique.length; i++){
        if(unique[i].profil !== id){
          newUnique.push(unique[i]);
        }
      }
      localStorage.setItem('attest_unique', JSON.stringify(newUnique));
    }


    let recurente = JSON.parse(localStorage.getItem('attest_recurente'));
    if (recurente !== null){
      let newRecurente = [];
      for (var i = 0; i < recurente.length; i++){
        if(recurente[i].profil !== id){
          newRecurente.push(recurente[i]);
        }
      }
      localStorage.setItem('attest_recurente', JSON.stringify(newRecurente));
    }

    for (let i = 0; i < this.users.length; i++){
      if (this.users[i].id === id){
        this.users.splice(i, 1);
        this.savUsers();
      }
    }
  }

  public updateUser(id: string, prenom: string, nom: string, dateNaissance: string, lieuNaissance: string, adresse: string, ville: string, codePostal: string, label: string){
    this.tempUser = {
      id,
      prenom,
      nom,
      dateNaissance,
      lieuNaissance,
      adresse,
      ville,
      codePostal,
      label,
    };
    for (let i = 0; i < this.users.length; i++){
      if (this.users[i].id === id){
        this.users[i] = this.tempUser;
        this.savUsers();
      }
    }
  }

  async error(msg){
    const alert = await this.alertCtrl.create({
      subHeader: msg,
      buttons: [{
        text: 'OK',
        cssClass: 'primary',
      }]
    });

    await alert.present();
  }
}

class User {
  id: string;
  prenom: string;
  nom: string;
  dateNaissance: string;
  lieuNaissance: string;
  adresse: string;
  ville: string;
  codePostal: string;
  label: string;
}
