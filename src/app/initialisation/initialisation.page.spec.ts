import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InitialisationPage } from './initialisation.page';

describe('InitialisationPage', () => {
  let component: InitialisationPage;
  let fixture: ComponentFixture<InitialisationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InitialisationPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InitialisationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
