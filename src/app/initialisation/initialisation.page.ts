import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {AuthService} from '../shared/service/auth.service';

@Component({
  selector: 'app-initialisation',
  templateUrl: './initialisation.page.html',
  styleUrls: ['./initialisation.page.scss'],
})
export class InitialisationPage implements OnInit {

  step = 0;
  userForm: FormGroup;
  user;

  constructor(
      private fb: FormBuilder,
      private auth: AuthService
  ) { }

  ngOnInit() {
    this.userForm = this.fb.group({
      prenom: ['', Validators.required],
      nom: ['', Validators.required],
      dateNaissance: ['', Validators.required],
      lieuNaissance: ['', Validators.required],
      adresse: ['', Validators.required],
      ville: ['', Validators.required],
      codePostal: ['', Validators.required],
      label: [''],
    });
  }

  addUser(){
    const data = this.userForm.value;
    if(data.label === null){
      data.label = '';
    }
    this.user = this.auth.getUser(this.auth.registrer(data.prenom, data.nom, data.dateNaissance, data.lieuNaissance, data.adresse, data.ville, data.codePostal, data.label));
    console.log(this.user);
    this.step++;
  }

  // affiche si un champ de formulaire est valide ou non
  setclassinput(input) {
    let css = '';
    if (this.userForm.get(input).touched && this.userForm.get(input).errors) {
      css = 'is-invalid';
    } else if (!this.userForm.get(input).errors) {
      css = 'is-valid';
    }
    return css;
  }

  displayValue(){
    let label = '';
    if(this.userForm.get('label').value !== null){
      label = this.userForm.get('label').value;
    }
    let prenom = '';
    if(this.userForm.get('prenom').value !== null){
      prenom = this.userForm.get('prenom').value;
    }
    let nom = '';
    if(this.userForm.get('nom').value !== null){
      nom = this.userForm.get('nom').value;
    }
    return prenom + ' ' + nom + ' ' + label;
  }
}
