import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import {AlertController} from '@ionic/angular';

import {AuthService} from '../../shared/service/auth.service';

import {faTrash, faEdit, faPlus, faTerminal, faMoon, faSun} from '@fortawesome/free-solid-svg-icons';
import {DarkmodeService} from '../../shared/service/darkmode.service';

@Component({
  selector: 'app-parametres',
  templateUrl: './parametres.page.html',
  styleUrls: ['./parametres.page.scss'],
})
export class ParametresPage implements OnInit {

  faFont = {
    faTrash,
    faEdit,
    faPlus,
    faTerminal,
    faMoon,
    faSun,
  };

  constructor(
      public auth: AuthService,
      public darkmode: DarkmodeService,
      public alertCtrl: AlertController,
  ) { }

  ionViewWillEnter(){
    this.ngOnInit();
  }
  ngOnInit() {
  }

  async confirmSuppr(id){
    const alert = await this.alertCtrl.create({
      subHeader: 'Voulez-vous vraiment supprimer ce profil ?',
      buttons: [{
        text: 'Annuler',
        cssClass: 'primary',
      },{
        text: 'Oui',
        cssClass: 'danger',
        handler: () => {
          this.auth.removeUser(id);
        }
      }]
    });

    await alert.present();
  }

}
