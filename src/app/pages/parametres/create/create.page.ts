import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {AuthService} from '../../../shared/service/auth.service';
import {ActivatedRoute, Router} from '@angular/router';
import {setclassinput} from '../../../app.component';

@Component({
  selector: 'app-create',
  templateUrl: './create.page.html',
  styleUrls: ['./create.page.scss'],
})
export class CreatePage implements OnInit {

  userForm: FormGroup;
  user: any;

  constructor(
      private fb: FormBuilder,
      private auth: AuthService,
      private router: Router,
      private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.user = this.auth.getUser(params['id']);
    });

    this.userForm = this.fb.group({
      prenom: [this.user.prenom, Validators.required],
      nom: [this.user.nom, Validators.required],
      dateNaissance: [this.user.dateNaissance, Validators.required],
      lieuNaissance: [this.user.lieuNaissance, Validators.required],
      adresse: [this.user.adresse, Validators.required],
      ville: [this.user.ville, Validators.required],
      codePostal: [this.user.codePostal, Validators.required],
      label: [this.user.label],
    });
  }

  addUser(){
    const data = this.userForm.value;
    if(data.label === null){
      data.label = '';
    }
    if(this.user.id === undefined){
      this.auth.registrer(data.prenom, data.nom, data.dateNaissance, data.lieuNaissance, data.adresse, data.ville, data.codePostal, data.label);
    } else {
      this.auth.updateUser(this.user.id, data.prenom, data.nom, data.dateNaissance, data.lieuNaissance, data.adresse, data.ville, data.codePostal, data.label);
    }
    this.router.navigate(['/parametres']);
  }

  setclassinput(input){
    return setclassinput(this.userForm, input);
  }

  displayValue(){
    let label = '';
    if(this.userForm.get('label').value !== null){
      label = this.userForm.get('label').value;
    }
    let prenom = '';
    if(this.userForm.get('prenom').value !== null){
      prenom = this.userForm.get('prenom').value;
    }
    let nom = '';
    if(this.userForm.get('nom').value !== null){
      nom = this.userForm.get('nom').value;
    }
    return prenom + ' ' + nom + ' ' + label;
  }
}
