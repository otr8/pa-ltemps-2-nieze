import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { UniquePage } from './unique.page';

import { Tab2PageRoutingModule } from './unique-routing.module';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';

@NgModule({
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        Tab2PageRoutingModule,
        ReactiveFormsModule,
        FontAwesomeModule
    ],
  declarations: [UniquePage]
})
export class Tab2PageModule {}
