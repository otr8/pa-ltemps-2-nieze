import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../shared/service/auth.service';
import {setclassinput} from '../../app.component';
import { MotifsService } from '../../shared/service/motifs.service';
import {BsModalService, BsModalRef} from 'ngx-bootstrap/modal';

import * as moment from 'moment';
import {Router} from '@angular/router';

@Component({
  selector: 'app-tab2',
  templateUrl: 'unique.page.html',
  styleUrls: ['unique.page.scss']
})
export class UniquePage implements OnInit{

  attestForm: FormGroup;
  lstProfils: any;
  raisons = this.motifService.getMotifs();
  motifsSelected = [];
  modalRef: BsModalRef;

  constructor(
      private fb: FormBuilder,
      private auth: AuthService,
      private modalService: BsModalService,
      private router: Router,
      public motifService: MotifsService,
  ) {}

  ionViewWillEnter(){
    this.ngOnInit();
  }
  ngOnInit() {
    this.motifsSelected = [];
    this.lstProfils = this.auth.getUsers();

    this.attestForm = this.fb.group({
      profil: ['', Validators.required],
      sortie: ['', Validators.required],
      motif: ['', Validators.required],
    },{
      Validators:[
        this.validcheck('motif')
      ]
    });
  }

  openModal(template) {
    this.modalRef = this.modalService.show(template);
  }

  validcheck(input){
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[input];

      if (control.errors && !control.errors.validcheck) {
        return;
      }

      if(this.motifsSelected === []){
        control.setErrors({ validcheck: true });
      }else{
        control.setErrors(null);
      }
    };
  }

  onCheckChange(value: string){
    const index = this.motifsSelected.indexOf(value);
    if(index > -1){
      this.motifsSelected.splice(index,1);
    }else{
      this.motifsSelected.push(value);
    }
  }

  addAttest(){

    var lstattest = JSON.parse(localStorage.getItem('attest_unique'));

    if(lstattest === null){
      lstattest = [];
    }
    lstattest.push({
      enr: moment().format(),
      profil: this.attestForm.value.profil,
      sortie: this.attestForm.value.sortie,
      motifs: this.motifsSelected,
    });

    localStorage.setItem('attest_unique', JSON.stringify(lstattest));
    this.router.navigate(['/']);
    this.attestForm.reset();
  }

  setclassinput(input){
    return setclassinput(this.attestForm, input);
  }
}
