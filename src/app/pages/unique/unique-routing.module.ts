import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UniquePage } from './unique.page';

const routes: Routes = [
  {
    path: '',
    component: UniquePage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Tab2PageRoutingModule {}
