import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { ExploreContainerComponentModule } from '../../explore-container/explore-container.module';

import { UniquePage } from './unique.page';

describe('Tab2Page', () => {
  let component: UniquePage;
  let fixture: ComponentFixture<UniquePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UniquePage],
      imports: [IonicModule.forRoot(), ExploreContainerComponentModule]
    }).compileComponents();

    fixture = TestBed.createComponent(UniquePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
