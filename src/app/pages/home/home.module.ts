import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HomePage } from './home.page';

import { Tab1PageRoutingModule } from './home-routing.module';
import {QRCodeModule} from 'angularx-qrcode';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {AccordionModule} from 'ngx-bootstrap/accordion';

@NgModule({
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        Tab1PageRoutingModule,
        QRCodeModule,
        FontAwesomeModule,
        AccordionModule
    ],
  declarations: [HomePage]
})
export class Tab1PageModule {}
