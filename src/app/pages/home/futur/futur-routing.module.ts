import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FuturPage } from './futur.page';

const routes: Routes = [
  {
    path: '',
    component: FuturPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FuturPageRoutingModule {}
