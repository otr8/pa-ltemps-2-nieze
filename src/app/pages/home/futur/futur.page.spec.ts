import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FuturPage } from './futur.page';

describe('FuturPage', () => {
  let component: FuturPage;
  let fixture: ComponentFixture<FuturPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FuturPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FuturPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
