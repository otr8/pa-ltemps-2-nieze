import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FuturPageRoutingModule } from './futur-routing.module';
import {QRCodeModule} from 'angularx-qrcode';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {AccordionModule} from 'ngx-bootstrap/accordion';

import { FuturPage } from './futur.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FuturPageRoutingModule,
    QRCodeModule,
    FontAwesomeModule,
    AccordionModule.forRoot()
  ],
  declarations: [FuturPage]
})
export class FuturPageModule {}
