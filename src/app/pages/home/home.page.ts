import { Component, OnInit } from '@angular/core';
import { MotifsService } from '../../shared/service/motifs.service';

import * as moment from 'moment';
import {AuthService} from '../../shared/service/auth.service';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';

import {faAddressCard, faUser, faBirthdayCake, faMapMarkedAlt, faClock, faDoorOpen} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-tab1',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage implements OnInit{

  faFont = {
    faAddressCard,
    faUser,
    faBirthdayCake,
    faMapMarkedAlt,
    faClock,
    faDoorOpen
  };

  raisonsAttestation = this.motifService.getMotifs();
  moment = moment;
  colorQr = '#263238FF';
  qrdata = '';
  qrW = window.innerWidth*0.8;
  lstAttestation = [];
  later = [];
  modalRef: BsModalRef;
  selectAttest: any;

  constructor(
      public auth: AuthService,
      private modalService: BsModalService,
      public motifService: MotifsService,
  ) {
  }

  ionViewWillEnter(){
    this.ngOnInit();
  }
  ngOnInit() {
    this.lstAttestation = [];
    this.later = [];
    this.getAttestUnique();
    this.getAttestRecurent();

    this.lstAttestation.sort(function compare(a, b) {
      if (a.sortie > b.sortie)
        return -1;
      if (a.sortie < b.sortie )
        return 1;
      return 0;
    });

    const temp = [];
    var aff = moment().add(1, 'hours');
    for (var i = 0; i< this.lstAttestation.length; i++){
      if(moment(this.lstAttestation[i].sortie).isAfter(aff)){
        this.later.push(this.lstAttestation[i]);
      }else{
        temp.push(this.lstAttestation[i]);
      }
    }
    this.lstAttestation = temp;
  }

  openModal(template, i) {
    this.modalRef = this.modalService.show(template);
    this.selectAttest = this.lstAttestation[i];
  }

  getAttestUnique(){
    let attest = JSON.parse(localStorage.getItem('attest_unique'));
    if(attest === null){
      attest = [];
    }

    let newAttest = [];
    const expiration = moment().subtract(24, 'hours');
    for (var i = 0; i < attest.length; i++){
      if(moment(attest[i].sortie).isAfter(expiration)){
        newAttest.push(attest[i]);
        this.lstAttestation.push(attest[i]);
      }
    }
    localStorage.setItem('attest_unique', JSON.stringify(newAttest));
  }

  getAttestRecurent(){
    let attest = JSON.parse(localStorage.getItem('attest_recurente'));
    if(attest === null){
      attest = [];
    }

    for (var i = 0; i < attest.length; i++){
      if(attest[i].jours.includes(moment().format('dddd'))){
        this.lstAttestation.push({
          enr: attest[i].enr,
          profil: attest[i].profil,
          sortie: moment(new Date()).format('YYYY-MM-DD') + ' ' + attest[i].sortie,
          motifs: attest[i].motifs,
        });
      }
    }
  }

  getqrTxt(i){
    const datas  = this.lstAttestation[i];
    const profil: any =  this.auth.getUser(datas.profil);

    const enr = moment(datas.enr).format('DD/MM/YYYY') + ' à ' + moment(datas.enr).format('HH:mm');
    const naissance = moment(profil.dateNaissance).format('DD/MM/YYYY') + ' à ' + profil.lieuNaissance;
    const sortie = moment(datas.sortie).format('DD/MM/YYYY') + ' à ' + moment(datas.sortie).format('HH:mm');

    const txt = 'Cree le:' + enr + ';Nom:' + profil.nom + ';Prenom:' + profil.prenom + ';Naissance:' + naissance + ';Adresse:' + profil.adresse + ' ' + profil.codePostal + ' ' + profil.ville + ';Sortie:' + sortie + ';Motifs:' + this.getLstMotifs(datas.motifs);
    return txt;
  }

  getLstMotifs(lst){
    var motifs = '';
    for (var i = 0; i < lst.length; i++){
      let prefix = '';
      if (i>0){
        prefix = ', ';
      }
      motifs = motifs + prefix + this.raisonsAttestation[lst[i]].value;
    }
    return motifs;
  }
}
