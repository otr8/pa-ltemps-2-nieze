import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RecurentePage } from './recurente.page';

import { Tab3PageRoutingModule } from './recurente-routing.module'
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {AccordionModule} from 'ngx-bootstrap/accordion';

@NgModule({
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        RouterModule.forChild([{path: '', component: RecurentePage}]),
        Tab3PageRoutingModule,
        FontAwesomeModule,
        AccordionModule,
    ],
  declarations: [RecurentePage]
})
export class Tab3PageModule {}
