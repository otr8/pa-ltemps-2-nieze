import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../../shared/service/auth.service';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {MotifsService } from '../../../shared/service/motifs.service';
import {setclassinput} from '../../../app.component';
import * as moment from 'moment';
import {Router} from '@angular/router';

@Component({
  selector: 'app-create',
  templateUrl: './create.page.html',
  styleUrls: ['./create.page.scss'],
})
export class CreatePage implements OnInit {

  attestForm: FormGroup;
  lstProfils: any;
  raisons = this.motifService.getMotifs();
  motifsSelected = [];
  jourSelected = [];
  modalRef: BsModalRef;

  constructor(
      private fb: FormBuilder,
      private auth: AuthService,
      private modalService: BsModalService,
      private router: Router,
      public motifService: MotifsService,
  ) { }

  ionViewWillEnter(){
    this.ngOnInit();
  }
  ngOnInit() {
    this.motifsSelected = [];
    this.lstProfils = this.auth.getUsers();

    this.attestForm = this.fb.group({
      titre: ['', Validators.required],
      profil: ['', Validators.required],
      jours: ['', Validators.required],
      sortie: ['', Validators.required],
      motif: ['', Validators.required],
    }, {
      Validators: [
          this.validcheck('motif'),
      ]
    });
  }

  setclassinput(input){
    return setclassinput(this.attestForm, input);
  }

  validcheck(input){
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[input];

      if (control.errors && !control.errors.validcheck) {
        return;
      }

      if(this.motifsSelected[0] === undefined || this.jourSelected[0] === undefined){
        control.setErrors({ validcheck: true });
      }else{
        control.setErrors(null);
      }
    };
  }

  onCheckChange(value: string, container){
    const index = container.indexOf(value);
    if(index > -1){
      container.splice(index,1);
    }else{
      container.push(value);
    }
  }

  openModal(template) {
    this.modalRef = this.modalService.show(template);
  }

  addAttest(){
    var lstattest = JSON.parse(localStorage.getItem('attest_recurente'));

    if(lstattest === null){
      lstattest = [];
    }
    lstattest.push({
      enr: moment().format(),
      titre: this.attestForm.value.titre,
      profil: this.attestForm.value.profil,
      sortie: this.attestForm.value.sortie,
      motifs: this.motifsSelected,
      jours: this.jourSelected,
    });

    localStorage.setItem('attest_recurente', JSON.stringify(lstattest));
    this.router.navigate(['/recurente']);
    this.attestForm.reset();
  }
}
