import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../shared/service/auth.service';
import { MotifsService } from '../../shared/service/motifs.service';

import {faPlus, faTrash} from '@fortawesome/free-solid-svg-icons';

import * as moment from 'moment';
import {AlertController} from '@ionic/angular';

@Component({
  selector: 'app-tab3',
  templateUrl: 'recurente.page.html',
  styleUrls: ['recurente.page.scss']
})
export class RecurentePage implements OnInit {

  faFont = {
    faPlus,
    faTrash,
  };
  moment = moment;
  raisonsAttestation = this.motifService.getMotifs();

  lstAttestation = [];

  constructor(
      public auth: AuthService,
      public alertCtrl: AlertController,
      public motifService: MotifsService,
  ) {}

  ionViewWillEnter(){
    this.ngOnInit();
  }

  ngOnInit() {
    this.lstAttestation = JSON.parse(localStorage.getItem('attest_recurente'));
  }

  async supprAttest(i){
    const alert = await this.alertCtrl.create({
      subHeader: 'Voulez-vous vraiment supprimer cette attestation ?',
      buttons: [{
        text: 'Annuler',
        cssClass: 'primary',
      },{
        text: 'Oui',
        cssClass: 'danger',
        handler: () => {
          this.lstAttestation.splice(i,1);
          localStorage.setItem('attest_recurente', JSON.stringify(this.lstAttestation));
          this.ngOnInit();
        }
      }]
    });

    await alert.present();
  }
}
