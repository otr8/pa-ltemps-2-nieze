import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { ExploreContainerComponentModule } from '../../explore-container/explore-container.module';

import { RecurentePage } from './recurente.page';

describe('Tab3Page', () => {
  let component: RecurentePage;
  let fixture: ComponentFixture<RecurentePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RecurentePage],
      imports: [IonicModule.forRoot(), ExploreContainerComponentModule]
    }).compileComponents();

    fixture = TestBed.createComponent(RecurentePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
