import { Component } from '@angular/core';

import {faFileAlt, faRoute, faRedoAlt, faCog} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  faFont = {
    faFileAlt,
    faRoute,
    faRedoAlt,
    faCog,
  };

  constructor() {}

}
