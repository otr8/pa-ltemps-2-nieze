import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';
import {pathToFileURL} from 'url';
import {AuthGuard} from '../shared/guard/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        redirectTo: 'home',
      },
      {
        path: 'home',
        loadChildren: () => import('../pages/home/home.module').then(m => m.Tab1PageModule)
      },
      {
        path: 'unique',
        loadChildren: () => import('../pages/unique/unique.module').then(m => m.Tab2PageModule)
      },
      {
        path: 'recurente',
        loadChildren: () => import('../pages/recurente/recurente.module').then(m => m.Tab3PageModule)
      },
      {
        path: 'parametres',
        loadChildren: () => import('../pages/parametres/parametres.module').then(m => m.ParametresPageModule)
      }
    ]
  },
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
